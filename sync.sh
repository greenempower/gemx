source env.sh

sudo -E rsync -av -f"+ */" -f"- *" "gemx" $GEMX_ROOT --progress # copy directory structure
sudo -E rsync -r gemx/* $GEMX_ROOT --progress # sync files
