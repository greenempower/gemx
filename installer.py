#!/usr/bin/python
# Linux installer for the GEMX subsystem

import os
import subprocess
import shutil, errno
import distutils

def configure():
    print("entering configure()")

def mkdir(path):
	if not os.path.exists(path):
		os.mkdir(path)
	else:
		print("mkdir(): '{path}' exists, skipping".format(path=path))

def create_dirs(dirs):
	for dir_ in dirs:
		mkdir(dir_)

def create_data_dir():
	print("Creating /data/")
	print("/data/ can be a sym link to another directory.")
	path = input("Which directory would you like /data/ to point to? (enter/return for none): ")

	if (len(path) == 0):
		mkdir("/opt/gemx/data")
	else:
		os.symlink("/opt/gemx/data", path)


def install_lib():
	#shutil.copytree("./lib", "/opt/gemx/lib/")
	pass

def schafold_global_system():
	mkdir("/usr/local/bin/gemx/")

def schafold():
	print("entering schafold()")
	print("sudo mkdir /opt/gemx")
	create_dirs(["/opt/gemx"])

	print("schafold(): populating dir")
	print("schafold(): creating /u/ ~ unix style dirs")
	mkdir("/opt/gemx/u")
	create_dirs([
		"/opt/gemx/u/tmp",
		"/opt/gemx/u/mnt",
		"/opt/gemx/u/etc",
		"/opt/gemx/u/bin",
		"/opt/gemx/u/lib",
		"/opt/gemx/u/virt"
	])

	mkdir("/opt/gemx/lib")
	mkdir("/opt/gemx/lib/python")

	#os.mkdir("/opt/gemx/net")
	#os.mkdir("/opt/gemx/web")

	print("schafold(): /domains/")
	mkdir("/opt/gemx/domains")

	print("schafold(): /x/")
	mkdir("/opt/gemx/x")
	mkdir("/opt/gemx/x/admin")
	mkdir("/opt/gemx/x/data")
	mkdir("/opt/gemx/x/daemons")
	mkdir("/opt/gemx/x/programs")
	mkdir("/opt/gemx/x/entropy")
	mkdir("/opt/gemx/x/hardware")
	mkdir("/opt/gemx/x/repos")
	mkdir("/opt/gemx/x/media")
	#os.mkdir("/opt/gemx/registry")

	print("schafold(): /work/")
	mkdir("/opt/gemx/work")
	mkdir("/opt/gemx/work/cad")
	mkdir("/opt/gemx/work/sim")
	mkdir("/opt/gemx/work/lab")
	mkdir("/opt/gemx/work/draft")
	mkdir("/opt/gemx/work/fin")

	#os.mkdir("/opt/gemx/de") # Desktop Environment

	#os.mkdir("/opt/gemx/auto")
	#os.mkdir("/opt/gemx/opti")
	#os.mkdir("/opt/gemx/games")

	#os.mkdir("/opt/gemx/style")
	#os.mkdir("/opt/gemx/themes")
	#os.mkdir("/opt/gemx/mods")

	print("schafold(): /kernel/")
	mkdir("/opt/gemx/kernel")

	print("schafold(): creating /dev/ ~ populating")
	mkdir("/opt/gemx/dev")
	mkdir("/opt/gemx/dev/docs")
	mkdir("/opt/gemx/dev/src")

	#os.mkdir("/opt/gemx/algos")
	#os.mkdir("/opt/gemx/social")
	#os.mkdir("/opt/gemx/mesh")
	#os.mkdir("/opt/gemx/ai")
	#os.mkdir("/opt/gemx/market")


def install_daemon():
	#shutil.copytree("./daemon", "/opt/gemx/x/daemons/system")
	#distutils.dir_util.copy_tree("./daemon", "/opt/gemx/x/daemons/system")
	mkdir("/opt/gemx/x/daemons/system")
	os.system("cp -rf ./daemon/* /opt/gemx/x/daemons/system")
	sym_dest = "/etc/systemd/system/gemx.service"
	if not os.path.exists(sym_dest):
		os.symlink("/opt/gemx/x/daemons/system/gemx.service", sym_dest)
	os.system("systemctl enable gemx")
	os.system("systemctl start gemx")

	hosts_file = open("/etc/hosts", "a")
	hosts_file.write("127.0.0.1   gemx")
	hosts_file.close()

def install():
	print("entering install()")
	install_lib()
	install_daemon()


configure()
schafold_global_system()
schafold()
install()
