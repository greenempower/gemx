import http.server
import socketserver

PORT = 63696

class Handler(http.server.BaseHTTPRequestHandler):
	def _set_headers(self):
		self.send_response(200)
		self.send_header("Content-type", "text/html")
		self.end_headers()

	def do_GET(self):
		self._set_headers()
		self.wfile.write("GEMX Web Server".encode("utf8"))

	def do_HEAD(self):
		self._set_headers()

#Handler = http.server.SimpleHTTPRequestHandler

with socketserver.TCPServer(("", PORT), Handler) as httpd:
    print("serving at port", PORT)
    httpd.serve_forever()
